# Etapa de dependencias
FROM node:18.14.2 as dependencies
# Root operations
ENV APP_DIR=/usr/src/app
RUN mkdir -p /usr/src/app
RUN chmod -R 777 /usr/src/app
WORKDIR ${APP_DIR}

# Only dependencies to help layer caching
COPY --chown=node:node package*.json ./
RUN npm install --legacy-peer-deps --force
RUN npm install -g @angular/cli

# Copiar archivos (esto debe estar en la misma etapa)
COPY --chown=root:root . .
RUN pwd
RUN ls -lah

# Etapa de construcción
FROM dependencies as builder
RUN ng build --configuration=production
RUN whoami

# Etapa final de la imagen
FROM nginx:stable-alpine
COPY --chown=nginx:nginx --from=builder /usr/src/app/dist /usr/share/nginx/app/
COPY --chown=nginx:nginx --from=dependencies /usr/src/app/nginx/nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80

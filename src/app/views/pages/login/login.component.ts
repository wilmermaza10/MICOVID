import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { LoginFormModel } from 'src/app/views/pages/model/LoginFormModel';
import { SessionService } from 'src/app/views/pages/services/session.service';
import { CryptoService } from 'src/app/utils/crypto.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  public loginForm: FormGroup = new LoginFormModel().formLogin();
  public isChecked: boolean = false;
  private cryptoService$ = new CryptoService();
  constructor(private loginSession$: SessionService, private router$: Router) {}

  sessionLogin(): void {
    if (!this.loginForm.invalid) {
      const encryptedData = this.cryptoService$
        .Encript(this.loginForm.get('password')?.value)
        .toString();
      const data = {
        Name: this.loginForm.get('username')?.value,
        Password: encryptedData,
      };
      this.loginSession$.sessionLogin(data).subscribe(() => {
        this.router$.navigate(['/dashboard']);
      });
    }
  }
}
